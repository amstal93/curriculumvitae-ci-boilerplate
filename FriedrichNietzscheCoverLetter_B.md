---
subject: My life as a soldier
author: F. Nietzsche
city: Naumburg
from:
- Artillerieregiment, 8. Batt.
- Nordstraße 15, Naumburg
to:
- Carl Freiherr von Gersdorff
- Stresow-Kaserne I
- Grenadierstraße 13–16
- 13597 Spandau

# Settings
mainfont: Linux Libertine O # fonts: Hoefler Text / OFL Sorts Mill Goudy / Sabon LT Std / Cardo / Linux Libertine O | you can use any good font, put your .otf in the fonts dir
lang: english # do not use capital letters: dutch / german
fontsize: 10pt # 10pt or 11pt or 12pt
customdate: 1990-01-02 # disabled for now
textcolor: "000000" # use hexcolor, 000000 for black
linkcolor: "1EAEDB" # use hexcolor, FF0000 for red
pagecolor: "FFFFFF" # use hexcolor, FFFFFF for white
# letterhead: true
# geometry: a4paper, left=35mm, right=35mm, top=50mm, bottom=25mm
geometry: a4paper, left=35mm, right=35mm, top=51mm, bottom=30mm
geometrysecondarypagestop: 11mm # this will be minus! So 51mm - 21mm
signaturefile: #signature.jpg # file plus extension like: signature.pdf or signature.jpg
signatureheight: 3 # is cm
donothidepersonalinformation: false # If your letterhead includes your personal information put in "false" or comment it out
letterhead: #LetterheadNormal.pdf # file plus extension like: letterhead.pdf
letterheadfront: LetterheadFrontPlus.pdf # file plus extension like: letterheadfront.pdf - To include a different letterhead on the first page
template: coverlettertemplate.tex
---

Dear Friend,

I am a bombardier in the second mounted division of the Fourth Horse Artillery.

You may well imagine how astonished I was by this revolution in my affairs, and what a violent upheaval it has made in my everyday humdrum existence. Nevertheless I have borne the change with determination and courage, and even derive a certain pleasure from this turn of fortune. Now that I have an opportunity of doing a little athletic training I am more than ever thankful to our Schopenhauer. For the first five weeks I had to be in the stables. At 5:30 in the morning I had to be among the horses, removing the manure and grooming the animals down with the currycomb and horse brush. For the present my work lasts on an average from 7 a.m. to 10 a.m. and from 11.30 a.m. to 6 p.m., the greater part of which I spend in parade drill. Four times a week we two soldiers who are to serve for a year have to attend a lecture given by a lieutenant, to prepare us for the reserve officers examination. You must know that in the horse artillery there is a tremendous amount to learn. We get most fun out of the riding lessons. My horse is a very fine animal, and I am supposed to have some talent for riding. When I and my steed gallop round the large parade ground, I feel very contented with my lot. On the whole, too, I am very well treated. Above all, we have a very nice captain.

You may well imagine how astonished I was by this revolution in my affairs, and what a violent upheaval it has made in my everyday humdrum existence. Nevertheless I have borne the change with determination and courage, and even derive a certain pleasure from this turn of fortune. Now that I have an opportunity of doing a little athletic training I am more than ever thankful to our Schopenhauer. For the first five weeks I had to be in the stables. At 5:30 in the morning I had to be among the horses, removing the manure and grooming the animals down with the currycomb and horse brush. For the present my work lasts on an average from 7 a.m. to 10 a.m. and from 11.30 a.m. to 6 p.m., the greater part of which I spend in parade drill. Four times a week we two soldiers who are to serve for a year have to attend a lecture given by a lieutenant, to prepare us for the reserve officers examination. You must know that in the horse artillery there is a tremendous amount to learn. We get most fun out of the riding lessons. My horse is a very fine animal, and I am supposed to have some talent for riding. When I and my steed gallop round the large parade ground, I feel very contented with my lot. On the whole, too, I am very well treated. Above all, we have a very nice captain.

You may well imagine how astonished I was by this revolution in my affairs, and what a violent upheaval it has made in my everyday humdrum existence. Nevertheless I have borne the change with determination and courage, and even derive a certain pleasure from this turn of fortune. Now that I have an opportunity of doing a little athletic training I am more than ever thankful to our Schopenhauer. For the first five weeks I had to be in the stables. At 5:30 in the morning I had to be among the horses, removing the manure and grooming the animals down with the currycomb and horse brush. For the present my work lasts on an average from 7 a.m. to 10 a.m. and from 11.30 a.m. to 6 p.m., the greater part of which I spend in parade drill. Four times a week we two soldiers who are to serve for a year have to attend a lecture given by a lieutenant, to prepare us for the reserve officers examination. You must know that in the horse artillery there is a tremendous amount to learn. We get most fun out of the riding lessons. My horse is a very fine animal, and I am supposed to have some talent for riding. When I and my steed gallop round the large parade ground, I feel very contented with my lot. On the whole, too, I am very well treated. Above all, we have a very nice captain.

I have now told you all about my life as a soldier. This is the reason why I have kept you waiting so long for news and for an answer to your last letter. Meanwhile, if I am not mistaken, you will probably have been freed from your military fetters; that is why I thought it would be best to address this letter to Spandau.

But my time is already up; a business letter to Volkmann and another to Ritschl have robbed me of much of it. So I must stop in order to get ready for the parade in full kit.

Well, old man, forgive my long neglect, and hold the god of War responsible for most of it.

Your devoted friend,
